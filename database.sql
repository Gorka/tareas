CREATE TABLE tareas(
    id-tarea            NUMERIC             PRIMARY KEY,
    fecha-limite        DATE,
    coste               NUMERIC,
    descripcion         TEXT
);
CREATE TABLE asignaciones(
    id-tarea            NUMERIC,
    id-persona          NUMERIC,
    PRIMARY KEY(id-tarea, id-persona)        PRIMARY KEY
);
CREATE TABLE bloqueos(
    id-bloqueante       NUMERIC,
    id-bloqueado        NUMERIC,
    PRIMARY KEY(id-bloqueante, id-bloqueado) PRIMARY KEY
);
CREATE TABLE personas(
    id-persona          NUMERIC             PRIMARY KEY,
    nombre              TEXT,
    apellido            TEXT
);

INSERT INTO tareas(
    (03-03, 2.50, "comprar patatas"),
    (03-03, 2.50, "comprar huevos"),
    (05-03, 0.00, "sacrificar una virgen a Quetzalcóalt"),
    (04-03, 0.00, "peparar tortilla de patata")
);
INSERT INTO personas(
    ("Gorka",       "Rodriguez"),
    ("Evil Gorka",  "Rodriguevil")
);
INSERT INTO asignaciones(
    (0,0),
    (1,0),
    (2,1),
    (3,0),
    (3,1)
);
INSERT INTO bloqueos(
    (0,3),
    (1,3)
);